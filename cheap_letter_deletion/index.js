const readline = require('node:readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

rl.question(`Enter a length of the string:`, length => {
    if (isNaN(length) || !length.length) {
        console.log('Please enter digital value!');
    }
    else {
        const string = randomizeString(length);
        const costs = randomizeCost(length);

        const result = findCostDeletion(string, costs);

        console.log('Original string:', string);
        console.log('Original costs:', costs);
        console.log();

        if (result.price) {
            console.log('Matches:', result.markedString);
        }

        console.log('Price:', result.price);
    }

    rl.close();
});

function randomizeString(length) {
    let result = '';
    const alphabet = 'abcdefghijklmnopqrstuvwxyz';
    const charactersLength = alphabet.length;

    while (result.length < length) {
        result += alphabet.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

function randomizeCost(length) {
    let arr = [];

    while (arr.length < length) {
        arr.push(Math.floor(Math.random() * 1000))
    }

    return arr;
}

function findCostDeletion(string, costs) {
    let price = 0;
    let indexes = [];

    [...string].forEach((char, index) => {
        if (string[index + 1] && char === string[index + 1]) {
            price += costs[index];
            indexes.push(index);
        }
    });

    let markedString = '',
        matchIndex = 0,
        redStart = '\x1b[31m',
        redEnd = '\x1b[0m';

    if (indexes.length) {
        indexes.forEach((indexInString, index) => {
            let matchedString;

            markedString += string.substring(matchIndex, indexInString);

            if (indexes[index + 1]) {
                if (indexes[index + 1] === indexInString + 1) {
                    matchedString = string[indexInString];
                    matchIndex = indexInString + 1;
                } else {
                    matchedString = string.substring(indexInString, indexInString+2);
                    matchIndex = indexInString + 2;
                }
            }
            else {
                matchedString = string.substring(indexInString, indexInString+2);
            }

            markedString += redStart + matchedString + redEnd;
        });

        markedString += string.substring(indexes[indexes.length - 1] + 2, string.length);
    } else {
        markedString = string;
    }

    return {price, markedString};
}