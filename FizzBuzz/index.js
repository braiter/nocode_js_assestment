const readline = require('node:readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

rl.question(`Enter a number:`, num => {
    let power = 0;
    const max = 1000;

    if (isNaN(num) || !num.length) {
        console.log('Please enter digital value!');
    }
    else if (num > 0 && num < max) {
        for (let i = 1; i < num; i++) {
            if (Math.pow(2, power) === i) {
                console.log('POWER');
                power++;
            } else {
                console.log(i)
            }
        }
    } else if (num > max) {
        console.log('The number is too large!');
    } else {
        console.log('The number must be greater than 0!');
    }


    rl.close();
});