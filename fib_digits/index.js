const readline = require('node:readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

rl.question(`Enter a index of a la Fib string:`, value => {
    let arr = [0, 1];
    const length = parseInt(value);
    const max = 1000000000;

    if (isNaN(num) || !value.length) {
        console.log('Please enter digital value!');
    }
    else if (length < max && length > 2) {
        for (let i = arr.length; i < length + 1; i++) {
            let firstAccu = 0,
                secondAccu = 0;

            const firstSum = arr[i - 2].toString().split('').reduce(
                (accumulator, currentValue) => parseInt(accumulator) + parseInt(currentValue),
                firstAccu,
            );

            const secondSum = arr[i - 1].toString().split('').reduce(
                (accumulator, currentValue) => parseInt(accumulator) + parseInt(currentValue),
                secondAccu,
            );

            arr.push(firstSum + secondSum);
        }

        console.log(`Numbers: ${arr.join(', ')}`);
        console.log(`Required number: ${arr[length]}`);
    }
    else if (length < 2) {
        console.log('The number must be greater than 2!');
    }
    else {
        console.log('The number is too large!');
    }

    rl.close();
});